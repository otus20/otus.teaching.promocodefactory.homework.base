﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<Guid> CreateAsync(T entity)
        {
           entity.Id = Guid.NewGuid();
           Data.Add(entity);

           return Task.FromResult(entity.Id);
        }

        public Task DeleteAsync(T entity)
        {
           Data.Remove(entity);
           return Task.CompletedTask;
        }

        public Task<T> UpdateAsync(T entity)
        {
           var itemIndex = Data.FindIndex(x => x.Id == entity.Id);

           if (itemIndex == -1)
              throw new InvalidOperationException("Entity not found");

           Data[itemIndex] = entity;
           return Task.FromResult(entity);
        }
    }
}